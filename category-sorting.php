<?php
/*
Plugin Name: Category Sorting
Plugin URI: http://baeldung.claaudius.com/
Description: Sorts categories
Author: Claudiu Sterian
Author URI: http://claaudius.com/
Version: 1.0
*/

add_action('admin_menu', 'category_sorting_admin');

function category_sorting_admin() {
  add_options_page('Category Sorting','Category Sorting','manage_options','category-sorting-settings','category_sorting_page');
}

function category_sorting_page() {
  if ( !current_user_can('manage_options') ) {
    wp_die( __("You don't have permission to access this.") );
  }
  
  $category_list = get_categories();
  
  //save values
  if ( isset($_POST['submit']) ) {
      foreach($category_list as $category){
        $option_name = 'category-sort-' . $category->term_id;  
        delete_option($option_name);
        if ( $_POST[$option_name] == '' ) {
          add_option($option_name, '0');
        } else {
          add_option($option_name, $_POST[$option_name]);
        }        
      }
  }  
  
  ?>
    <div class="wrap">
      <h1>Category Sorting</h1>
      <?php $category_list = get_categories(); ?>      
      <form action="" method="post">
        <table class="wp-list-table widefat fixed striped tags" style="width: 500px; margin-top: 15px;">
          <thead>
            <tr>
              <th style="width: 60px;">ID</th>
              <th>Category</th>
              <th>Sort Order</th>
            </tr>
          </thead>          
          <?php foreach($category_list as $category){ ?>            
            <tr>
              <td style="width: 60px;"><?=$category->term_id?></td>
              <td><?=$category->name?></td>
              <td>
                <input type="text" value="<?=get_option('category-sort-'.$category->term_id)?>" name="category-sort-<?=$category->term_id?>" style="width: 50px;" />                
              </td>
            </tr>
          <?php } ?>
        </table>
        <input type="hidden" name="submit" value="submit" />
        <input type="submit" value="Save" class="button button-primary" style="margin-top: 15px;" />
      </form>
    </div>
  <?php
}
?>
